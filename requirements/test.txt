pytest~=6.0
pytest-spec~=3.0
pytest-cov~=2.10
pytest-mock~=3.3
safety
webtest
liccheck~=0.1

## Code style tools
black~=22.3
flake8==3.7.9
isort~=5.10
